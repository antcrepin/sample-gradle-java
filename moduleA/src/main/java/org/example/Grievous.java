package org.example;

public class Grievous {

    private Grievous() {}

    public static String answer(String greeting) {
        if (greeting.equals("Hello there!")) {
            return "Hum, General Kenobi!";
        } else {
            return "???";
        }
    }
}