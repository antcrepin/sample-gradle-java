package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SyracuseTests {

    @Test
    void test() {
        Assertions.assertEquals(3, Syracuse.next(6));
        Assertions.assertEquals(16, Syracuse.next(5));
    }
}
