package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class GrievousTests {

    @Test
    void test() {
        Assertions.assertEquals("???", Grievous.answer("Hello!"));
        Assertions.assertEquals("Hum, General Kenobi!", Grievous.answer("Hello there!"));
    }
}
