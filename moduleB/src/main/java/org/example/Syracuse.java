package org.example;

public class Syracuse {

    private Syracuse() {}

    public static int next(int n) {
        return (n % 2 == 0) ? n / 2 : 3 * n + 1;
    }
}